'use strict';

/**
 * @ngdoc function
 * @name algorajaFrontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the algorajaFrontendApp
 */
angular.module('algorajaFrontendApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
