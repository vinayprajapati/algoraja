'use strict';

/**
 * @ngdoc function
 * @name algorajaFrontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the algorajaFrontendApp
 */
angular.module('algorajaFrontendApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
